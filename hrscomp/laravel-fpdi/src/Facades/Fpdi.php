<?php

namespace HRSComp\Fpdf\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Fpdf.
 */
class Fpdi extends Facade
{
    /**
     * Get the registered component name.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fpdi';
    }
}
